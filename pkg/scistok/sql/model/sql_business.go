package model

import "scistok/pkg/scistok/domain"

const businessTableName = "business"

type SqlBusiness struct {
	ID        uint64 `gorm:"primaryKey"`
	UUID      string `gorm:"column:uuid;unique"`
	Name      string
	City      string
	Thematic  string
	Contracts []string `gorm:"type:text[]"`
}

// TableName returns the name of the table
func (ths SqlBusiness) TableName() string {
	return businessTableName
}

func NewSqlBusiness(business *domain.Business) *SqlBusiness {
	return &SqlBusiness{
		UUID:      business.UUID.Str(),
		Name:      business.Name,
		City:      business.City,
		Thematic:  business.Thematic,
		Contracts: business.Contracts,
	}
}

func (ths *SqlBusiness) ToDomainBusiness() *domain.Business {
	return &domain.Business{
		UUID:      domain.UUID(ths.UUID),
		Name:      ths.Name,
		City:      ths.City,
		Thematic:  ths.Thematic,
		Contracts: ths.Contracts,
	}
}
