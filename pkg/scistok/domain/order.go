package domain

type Order struct {
	UUID         UUID   `json:"uuid"`
	BusinessUUID UUID   `json:"business_uuid"`
	Name         string `json:"name"`
	City         string `json:"city"`
	Thematic     string `json:"thematic"`
	ShortInfo    string `json:"short_info"`
	FullInfo     string `json:"full_info"`
}

type GetOrder struct {
	UUID         UUID     `json:"uuid"`
	BusinessUUID UUID     `json:"business_uuid"`
	Name         string   `json:"name"`
	City         string   `json:"city"`
	Thematic     string   `json:"thematic"`
	ShortInfo    string   `json:"short_info"`
	FullInfo     string   `json:"full_info"`
	Business     Business `json:"business"`
}
