package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/usecase"
)

// GetLaboratoriesHandler is handler for usecase.GetLaboratories
func (c Controller) GetLaboratoriesHandler(usecase usecase.GetLaboratories) gin.HandlerFunc {
	return func(context *gin.Context) {
		laboratories, err := usecase.Execute()
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		context.JSON(http.StatusOK, laboratories)
	}
}
