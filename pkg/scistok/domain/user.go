package domain

// User defines user object structure
type User struct {
	UserUUID     UUID   `json:"user_uuid"`
	Email        string `json:"email"`
	PasswordHash string `json:"password_hash"`
	Role         string `json:"role"`
}
