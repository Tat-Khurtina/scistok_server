package domain

type Business struct {
	UUID      UUID     `json:"uuid"`
	Name      string   `json:"name"`
	City      string   `json:"city"`
	Thematic  string   `json:"thematic"`
	Mail      string   `json:"mail"`
	Contracts []string `json:"contracts"`
}
