package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/domain"
	"scistok/pkg/scistok/usecase"
)

// GetBusinessHandler is handler for usecase.GetBusiness
func (c Controller) GetBusinessHandler(usecase usecase.GetBusinessByUUID) gin.HandlerFunc {
	return func(context *gin.Context) {
		UUID := context.Param("uuid")
		business, err := usecase.Execute(domain.UUID(UUID))
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		context.JSON(http.StatusOK, business)
	}
}
