package config

import (
	"github.com/urfave/cli/v2"
	"scistok/app/cmd/config/env"
)

// Config a general config for app
type Config struct {
	Env env.Config
}

// CreateConfig read a context to Config
func CreateConfig(c *cli.Context) Config {
	return Config{
		Env: env.CreateConfig(c),
	}
}
