package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/domain"
	"scistok/pkg/scistok/usecase"
)

// GetLaboratoryHandler is handler for usecase.GetLaboratory
func (c Controller) GetLaboratoryHandler(usecase usecase.GetLaboratoryByUUID) gin.HandlerFunc {
	return func(context *gin.Context) {
		UUID := context.Param("uuid")
		laboratory, err := usecase.Execute(domain.UUID(UUID))
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		context.JSON(http.StatusOK, laboratory)
	}
}
