package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/domain"
	"scistok/pkg/scistok/usecase"
)

// CreateOrderHandler is handler for usecase.CreateOrder
func (c Controller) CreateOrderHandler(usecase usecase.CreateOrder) gin.HandlerFunc {
	return func(context *gin.Context) {
		var order domain.Order

		err := context.BindJSON(&order)
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		orderUUID, err := usecase.Execute(order)
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}

		context.JSON(http.StatusOK, orderUUID)
	}
}
