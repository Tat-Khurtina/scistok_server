package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (c Controller) PageLaboratoryHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		uuid := context.Param("uuid")

		context.HTML(
			// Зададим HTTP статус 200 (OK)
			http.StatusOK,

			"lab.html",
			// Передадим данные в шаблон
			gin.H{
				"uuid": uuid,
			},
		)
	}
}
