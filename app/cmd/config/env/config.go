package env

import "github.com/urfave/cli/v2"

// Config a config for env of app
type Config struct {
	ListenAddr string
	Env        string
}

// CreateConfig read a context to Config
func CreateConfig(c *cli.Context) Config {
	return Config{
		ListenAddr: c.String(LISTEN),
		Env:        c.String(ENV),
	}
}
