package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/usecase"
)

// GetOrdersHandler is handler for usecase.GetOrders
func (c Controller) GetOrdersHandler(usecase usecase.GetOrders) gin.HandlerFunc {
	return func(context *gin.Context) {
		orders, err := usecase.Execute()
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		context.JSON(http.StatusOK, orders)
	}
}
