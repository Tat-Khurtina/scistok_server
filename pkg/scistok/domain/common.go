package domain

type UUID string

// Str converts UUID to string
func (ths UUID) Str() string {
	return string(ths)
}
