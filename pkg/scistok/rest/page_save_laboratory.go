package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (c Controller) PageSaveLaboratoryHandler() gin.HandlerFunc {
	return func(context *gin.Context) {

		context.HTML(
			// Зададим HTTP статус 200 (OK)
			http.StatusOK,

			"save_lab.html",
			// Передадим данные в шаблон
			gin.H{},
		)
	}
}
