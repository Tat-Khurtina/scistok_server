package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"scistok/pkg/scistok/usecase"
)

// GetBusinessesHandler is handler for usecase.GetBusinesses
func (c Controller) GetBusinessesHandler(usecase usecase.GetBusinesses) gin.HandlerFunc {
	return func(context *gin.Context) {
		businesses, err := usecase.Execute()
		if err != nil {
			context.JSON(http.StatusInternalServerError, nil)
			return
		}
		context.JSON(http.StatusOK, businesses)
	}
}
