package domain

type Laboratory struct {
	UUID            UUID     `json:"uuid"`
	Name            string   `json:"name"`
	University      string   `json:"university"`
	City            string   `json:"city"`
	Competencies    string   `json:"competencies"`
	Thematic        string   `json:"thematic"`
	Director        string   `json:"director"`
	Rating          uint64   `json:"rating"`
	Language        string   `json:"language"`
	Specialist      string   `json:"specialist"`
	AreasOfActivity string   `json:"areas_of_activity"`
	Contracts       []string `json:"contracts"`
}
