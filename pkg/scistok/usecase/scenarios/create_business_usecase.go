package scenarios

import (
	"scistok/pkg/scistok/domain"
	"scistok/pkg/scistok/usecase/access"
)

// CreateBusinessUseCase use case for domain.Business
type CreateBusinessUseCase struct {
	persist access.Persist
}

// NewCreateBusinessUseCase create a new CreateBusinessUseCase
func NewCreateBusinessUseCase(persist access.Persist) CreateBusinessUseCase {
	return CreateBusinessUseCase{persist: persist}
}

// Execute method for CreateBusinessUseCase
func (ths CreateBusinessUseCase) Execute(business domain.Business) (string, error) {
	return ths.persist.CreateBusiness(business)
}
