package env

import (
	"github.com/urfave/cli/v2"
)

// Envs flags to set up environment
var Envs = []cli.Flag{
	&cli.StringFlag{
		Name:  LISTEN,
		Value: ":9990",
		Usage: "address where app will listen",
	},
	&cli.StringFlag{
		Name:  ENV,
		Value: "dev",
		Usage: "environment",
	},
}
