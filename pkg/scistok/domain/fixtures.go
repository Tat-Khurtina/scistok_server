package domain

import (
	"github.com/jaswdr/faker"
	"math/rand"
	"time"
)

// TechnicalBusiness creates a new Technical Business
func TechnicalBusiness() Business {
	return Business{
		UUID:     "00000000-0000-0000-0000-000000000000",
		Name:     "Технический аккаунт",
		City:     "Москва",
		Thematic: "Информационные технологии",
		Mail:     "technical@mail.ru",
		//Contracts: []string{"89090009090", "tech@mail.ru"},
	}
}

// FakeUUID generates a fake UUID
func FakeUUID() string {
	f := faker.NewWithSeed(rand.NewSource(time.Now().UnixNano()))
	uuid := f.UUID().V4()
	uuid = uuid[:8] + "-" + uuid[8:12] + "-" + uuid[12:16] + "-" + uuid[16:20] + "-" + uuid[20:]
	return uuid
}

// FakeLaboratory creates a new fake Laboratory
func FakeLaboratory2() Laboratory {
	return Laboratory{
		UUID:            UUID(FakeUUID()),
		Name:            "Институт Астрофизики",
		University:      "НИЯУ МИФИ",
		City:            "Москва",
		Competencies:    "Влияние излучений на околоземное пространство и на верхние слои Земной атмосферы",
		Thematic:        "Астрофизика",
		Director:        "Александр Александрович",
		Rating:          0,
		Language:        "RU",
		Specialist:      "Специалист 1 - описание, специалист 2 - описание",
		AreasOfActivity: "исследование процессов накопления энергии и ее трансформации в энергию ускоренных частиц во время солнечных вспышек, изучение механизмов ускорения, распространения и взаимодействия энергичных частиц в атмосфере Солнца, исследование корреляции солнечной активности с физико-химическими процессами в верхней атмосфере Земли",
		Contracts:       nil,
	}
}

// FakeLaboratory creates a new fake Laboratory
func FakeLaboratory() Laboratory {
	return Laboratory{
		UUID:            UUID(FakeUUID()),
		Name:            "Центр технологий нуклидных систем",
		University:      "НИЯУ МИФИ",
		City:            "Москва",
		Competencies:    "Создание и использование нуклидных систем, обращение с ядерным топливом, другими ядерными материалами и изотопными продуктами.",
		Thematic:        "Физика",
		Director:        "Александр Александрович",
		Rating:          0,
		Language:        "RU",
		Specialist:      "Специалист 1 - описание, специалист 2 - описание",
		AreasOfActivity: "ядерные энерготехнологии, нуклидные системы ",
		Contracts:       nil,
	}
}

// FakeBusiness creates a new fake Business
func FakeBusiness() Business {
	return Business{
		UUID:     UUID(FakeUUID()),
		Name:     "Финтех Хаб",
		City:     "Москва",
		Thematic: "Информационные технологии",
		Mail:     "mail@mail.ru",
		//Contracts: []string{"89090009090", "f@mail.ru"},
	}
}

// FakeBusiness creates a new fake Business
func FakeBusiness2() Business {
	return Business{
		UUID:     UUID(FakeUUID()),
		Name:     "ООО Организация",
		City:     "Москва",
		Thematic: "Информационные технологии",
		Mail:     "mail@mail.ru",
		//Contracts: []string{"87770009090", "f@mail.ru"},
	}
}

// FakeOrder creates a new fake Order
func FakeOrder() Order {
	return Order{
		UUID:         UUID(FakeUUID()),
		BusinessUUID: UUID(FakeUUID()),
		Name:         "Управление формированием ядерного энергоресурсного баланса",
		City:         "Москва",
		Thematic:     "Физика",
		ShortInfo:    "Создание аппаратно-программного комплекса для управления формированием ядерного энергоресурсного баланса перспективной атомной энергетики",
		FullInfo:     "Выполнить работу по теме «Разработка технологии оценки эффективности хранения, транспортировки и переработки ОЯТ и РАО, создание аппаратно-программного комплекса для управления формированием ядерного энергоресурсного баланса перспективной атомной энергетики» в рамках ФЦП «Национальная технологическая база» на 2007 – 2011 годы, направление «Технологии ядерной энергетики нового поколения».",
	}
}

// FakeOrder creates a new fake Order
func FakeOrder2() Order {
	return Order{
		UUID:         UUID(FakeUUID()),
		BusinessUUID: UUID(FakeUUID()),
		Name:         "Нейтринного детектор нового поколения",
		City:         "Москва",
		Thematic:     "Физика",
		ShortInfo:    "Разработка нейтринного детектора нового поколения, использующего эффект когерентного рассеяния на тяжелых ядрах ксенона для повышения чувствительности к реакторным антинейтрино.",
		FullInfo:     "В рамках данного проекта планируется провести модельный опыт, демонстрирующий возможность регистрировать ядра отдачи ксенона с энергиями меньше 1 кэВ с помощью эмиссионной детекторной технологии в образцах жидкого ксенона массой несколько килограммов. Опыт будет поставлен с помощью модели эмиссионного детектора на квазимонохроматическом нейтронном пучке, формируемом с помощью интерференционных фильтров на горизонтальном экспериментальном канале ГЭК-10 исследовательского реактора ИРТ МИФИ.",
	}
}
