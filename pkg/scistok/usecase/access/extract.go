package access

import "scistok/pkg/scistok/domain"

// Extract an extract interface for repo
type Extract interface {
	GetLaboratoryByUUID(UUID domain.UUID) (*domain.Laboratory, error)
	GetLaboratories() ([]domain.Laboratory, error)
	GetBusinessByUUID(UUID domain.UUID) (*domain.Business, error)
	GetBusinesses() ([]domain.Business, error)
	GetOrderByUUID(UUID domain.UUID) (*domain.Order, error)
	GetOrders() ([]domain.Order, error)
}
