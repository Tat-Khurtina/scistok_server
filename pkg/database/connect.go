package database

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"scistok/pkg/scistok/domain"
	"scistok/pkg/scistok/sql/model"
)

// InitPostgresDB init a postgres db
func InitPostgresDB() (*gorm.DB, error) {
	dsn := "sqlite.db"
	db, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		return nil, err
	}

	err = migrateData(db)
	if err != nil {
		return nil, err
	}

	//err = createTechnicalData(db)
	//if err != nil {
	//	return nil, err
	//}
	//
	//err = initializeTestData(db)
	//if err != nil {
	//	return nil, err
	//}

	return db, nil
}

func migrateData(db *gorm.DB) error {
	return db.AutoMigrate(
		&model.SqlOrder{},
		&model.SqlLaboratory{},
		&model.SqlBusiness{},
		&model.SqlUser{},
	)
}

func createTechnicalData(db *gorm.DB) error {
	business := domain.TechnicalBusiness()
	sqlBusiness := model.NewSqlBusiness(&business)
	if err := db.Create(sqlBusiness).Error; err != nil {
		return err
	}

	return nil
}

func initializeTestData(db *gorm.DB) error {

	laboratory := domain.FakeLaboratory()
	sqlLaboratory := model.NewSqlLaboratory(&laboratory)
	if err := db.Create(sqlLaboratory).Error; err != nil {
		return err
	}

	laboratory = domain.FakeLaboratory2()
	sqlLaboratory = model.NewSqlLaboratory(&laboratory)
	if err := db.Create(sqlLaboratory).Error; err != nil {
		return err
	}

	business := domain.FakeBusiness()
	sqlBusiness := model.NewSqlBusiness(&business)
	if err := db.Create(sqlBusiness).Error; err != nil {
		return err
	}

	order := domain.FakeOrder()
	order.BusinessUUID = business.UUID
	sqlOrder := model.NewSqlOrder(&order)
	if err := db.Create(sqlOrder).Error; err != nil {
		return err
	}

	business = domain.FakeBusiness2()
	sqlBusiness = model.NewSqlBusiness(&business)
	if err := db.Create(sqlBusiness).Error; err != nil {
		return err
	}

	order = domain.FakeOrder()
	order.BusinessUUID = business.UUID
	sqlOrder = model.NewSqlOrder(&order)
	if err := db.Create(sqlOrder).Error; err != nil {
		return err
	}

	return nil
}
