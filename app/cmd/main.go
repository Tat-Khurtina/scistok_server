package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"scistok/app/cmd/config"
	"scistok/app/cmd/config/env"
	"scistok/pkg/database"
	"scistok/pkg/scistok/rest"
)

func main() {
	app := &cli.App{
		Name:   "SciStok service",
		Flags:  append(env.Envs),
		Action: run,
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatalln(err)
	}
}

func run(c *cli.Context) error {
	log.Println("app started")
	cfg := config.CreateConfig(c)

	db, err := database.InitPostgresDB()
	if err != nil {
		return err
	}
	log.Println("connection with db is successful")

	engine := gin.Default()
	engine.Use(cors.Default())
	engine.LoadHTMLGlob("templates/*")
	//engine.Use(static.Serve("/css", static.LocalFile("css", true)))
	//engine.Use(static.Serve("/images", static.LocalFile("images", true)))
	//engine.Use(static.Serve("/js", static.LocalFile("js", true)))
	engine.Use(static.Serve("/assets", static.LocalFile("assets", true)))

	// set route handlers
	apiGroup := engine.Group("/api")

	// api version
	v1Group := apiGroup.Group("/v1")
	fmt.Println(v1Group)

	rest.NewAPI(v1Group, db).Handle()
	log.Println("API handled successful")
	err = engine.Run(cfg.Env.ListenAddr)
	if err != nil {
		return err
	}
	log.Println("app finished")
	return nil
}
